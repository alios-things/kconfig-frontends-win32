@echo off
set KCONFIG_CONFIG=%AOS_CONFIG%
set KCONFIG_AUTOCONFIG=%AOS_CONFIG_DIR%/auto.conf
set KCONFIG_AUTOHEADER=%AOS_CONFIG_DIR%/autoconf.h
set KCONFIG_TRISTATE=%AOS_CONFIG_DIR%/tristate.conf
set KCONFIG_MCONF=%KCONFIG_MCONF:bat=exe%
chcp 437
%KCONFIG_MCONF% %1
